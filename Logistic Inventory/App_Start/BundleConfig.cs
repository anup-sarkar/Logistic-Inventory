﻿using System.Web;
using System.Web.Optimization;

namespace Logistic_Inventory
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/js/core/jquery.min.js",
                        "~/Scripts/js/core/popper.min.js",
                        "~/Scripts/js/core/bootstrap.min.js",
                        "~/Scripts/js/plugins/perfect-scrollbar.jquery.min.js",
                        "~/Scripts/js/plugins/chartjs.min.js",
                          "~/Scripts/js/plugins/bootstrap-notify.js",
                            "~/Scripts/js/black-dashboard.min.js",
                              "~/Scripts/js/demo.js"
 

                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/nucleo-icons.css",
                       "~/Content/css/black-dashboard.css",
                        "~/Content/demo/demo.css",


                      "~/Content/site2.css"));
        }
    }
}
