﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Logistic_Inventory.Models
{
    public class Products
    {
        public int id { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "Product name cannot be longer than 150 characters.")]
        public String ProductName { get; set; }

        [Required]
        [StringLength(1000, MinimumLength = 7, ErrorMessage = "Product Details cannot be longer than 1000 characters.")]
        public String Description { get; set; }


        [StringLength(100, ErrorMessage = "Manufacture name cannot be longer than 100 characters.")]
        public String Manufacturer { get; set; }


        [StringLength(50, ErrorMessage = "Product Model cannot be longer than 50 characters.")]
        public String Model { get; set; }


        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal UnitPrice { get; set; }



    }
}