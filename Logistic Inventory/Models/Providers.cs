﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Logistic_Inventory.Models
{
    public class Providers
    {
        public int id { get; set; }

        [Required]
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }


        [Required]
        [DisplayName("Company Address")]
        public string  Address { get; set; }


        [Required]
        [DisplayName("Company Email")]
        public string  Email { get; set; }

        
        [DisplayName("Company Phone")]
        public string  Phone { get; set; }



        [Required]
        [DisplayName("Mobile")]
        [RegularExpression(@"^([0-9]{11})$", ErrorMessage = "Error: Please enter a valid mobile number, Only 11 digits allowed")]
        public string Mobile { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Date OF Joining")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime? JoiningDate { get; set; }



        [DisplayName("Bank Account No.")]
        public string BankAcc { get; set; }

        [DisplayName("TIN")]
        public string TIN { get; set; }

        [DisplayName("Dealing Person Name")]
        public string DealingPerson { get; set; }

        

        public string Comment { get; set; }
    }
}