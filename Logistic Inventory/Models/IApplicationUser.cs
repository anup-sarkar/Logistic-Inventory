﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Logistic_Inventory.Models
{
    interface IApplicationUser
    {
        string Name { get; set; }
        string UserRole { get; set; }

        string BranchCode { get; set; }


        Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager);
    }
}
