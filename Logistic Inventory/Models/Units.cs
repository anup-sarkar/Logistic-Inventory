﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Logistic_Inventory.Models
{
    public class Units
    {
        public int id { get; set; }



        [Required]
        [DisplayName("Unit Short Name")]
        public String UnitShortName { get; set; }



        [Required]
        [DisplayName("Unit Full Name")]
        public String UnitFullName { get; set; }


        public String Details { get; set; }


        [DisplayName("Sort Order")]
        public int SortOrder { get; set; }
    }
}