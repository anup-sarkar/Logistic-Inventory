﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Logistic_Inventory.Models
{
    public class Branch
    {
        public int id { get; set; }

        [Required]
        [DisplayName("Branch Code")]
        [Range(0,999)]
        public string BranchCode { get; set; }

        [Required]
        [DisplayName("Branch Name")]
        public string BranchName { get; set; }

        public string Comment { get; set; }
    }
}