﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Logistic_Inventory.Models
{
    public class Category
    {
        public int id { get; set; }



        [Required]
        [DisplayName("Category Name")]
        public String CatName { get; set; }


         
        public String Details { get; set; }


        [DisplayName("Sort Order")]
        public int SortOrder { get; set; }


       
    }
}