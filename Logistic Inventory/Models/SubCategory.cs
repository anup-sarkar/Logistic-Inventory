﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Logistic_Inventory.Models
{
    public class SubCategory
    {
        public int id { get; set; }



        [Required]
        [DisplayName("Sub Category Name")]
        public String SubCatName { get; set; }

        public String Details { get; set; }


        public int SortOrder { get; set; }

        public int? CategoryID { get; set; }

        public Category category { get; set; }
    }
}