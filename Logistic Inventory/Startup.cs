﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Logistic_Inventory.Startup))]
namespace Logistic_Inventory
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
