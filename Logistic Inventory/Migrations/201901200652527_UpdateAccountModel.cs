namespace Logistic_Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAccountModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Name", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserRole", c => c.String(nullable: false));
            AddColumn("dbo.AspNetUsers", "BranchCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "BranchCode");
            DropColumn("dbo.AspNetUsers", "UserRole");
            DropColumn("dbo.AspNetUsers", "Name");
        }
    }
}
