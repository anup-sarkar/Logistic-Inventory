namespace Logistic_Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBranch : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        BranchCode = c.String(),
                        BranchName = c.String(),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Branches");
        }
    }
}
