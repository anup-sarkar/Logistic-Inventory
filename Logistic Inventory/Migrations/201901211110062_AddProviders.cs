namespace Logistic_Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProviders : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Providers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Phone = c.String(),
                        Mobile = c.String(nullable: false),
                        BankAcc = c.String(),
                        TIN = c.String(),
                        DealingPerson = c.String(),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            AlterColumn("dbo.Branches", "BranchCode", c => c.String(nullable: false));
            AlterColumn("dbo.Branches", "BranchName", c => c.String(nullable: false));
            AlterColumn("dbo.Categories", "CatName", c => c.String(nullable: false));
            AlterColumn("dbo.SubCategories", "SubCatName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SubCategories", "SubCatName", c => c.String());
            AlterColumn("dbo.Categories", "CatName", c => c.String());
            AlterColumn("dbo.Branches", "BranchName", c => c.String());
            AlterColumn("dbo.Branches", "BranchCode", c => c.String());
            DropTable("dbo.Providers");
        }
    }
}
