// <auto-generated />
namespace Logistic_Inventory.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class addUnit : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addUnit));
        
        string IMigrationMetadata.Id
        {
            get { return "201901220404422_addUnit"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
