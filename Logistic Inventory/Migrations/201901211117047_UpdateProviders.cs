namespace Logistic_Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProviders : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Providers", "JoiningDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Providers", "JoiningDate");
        }
    }
}
